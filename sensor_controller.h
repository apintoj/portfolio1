#ifndef SENSOR_CONTROLLER_H_INCLUDED
#define SENSOR_CONTROLLER_H_INCLUDED

class sensor_controller{
   public:
      // pure virtual function providing interface framework.
      virtual int read_data() = 0;
      virtual int read_type() = 0;
      virtual int get_status() = 0;
};

// Derived classes
class temperature_sensor: public sensor_controller {
   public:
      int read_data(){
         return 0; //pin reading code to be written later
      }
      int read_type(){
          return 0;//pin reading code to be written later
      }
      int get_status(){
          return 0;//pin reading code to be written later

      }
};

class humidity_sensor: public sensor_controller {
   public:
      int read_data(){
         return 0; //pin reading code to be written later
      }
      int read_type(){
          return 0;//pin reading code to be written later
      }
      int get_status(){
          return 0;//pin reading code to be written later

      }
};


#endif // SENSOR_CONTROLLER_H_INCLUDED
