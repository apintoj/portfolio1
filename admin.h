#ifndef ADMIN_H_INCLUDED
#define ADMIN_H_INCLUDED

class admin {
   public:
      virtual int get_schedule() = 0;
};

#endif // ADMIN_H_INCLUDED